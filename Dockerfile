FROM docker.io/library/alpine
MAINTAINER Elias Meyer

ENV TZ=Europe/Helsinki

RUN apk update &&       \
    apk upgrade &&      \
    apk add git wget tree vim busybox-suid tzdata

# Install python/pip
ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools urllib3

# Add user
RUN addgroup ddns &&    \
    adduser -G ddns -S -D -H ddns

COPY ./src /DDNS/src
COPY ./cfg /DDNS/cfg
RUN chown -R ddns:ddns /DDNS

USER ddns

# Run the command on container startup
CMD ["python", "/DDNS/src/main.py"]

