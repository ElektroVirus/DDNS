#!/bin/bash
podman build -t ddns -f Dockerfile
podman container run -d --name ddns localhost/ddns

podman generate systemd --files --name ddns
mkdir -p ~/.config/systemd/user
cp container-ddns.service ~/.config/systemd/user/
systemctl --user daemon-reload
systemctl --user enable --now container-ddns.service
