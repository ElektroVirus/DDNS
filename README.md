## Synopsis

This project aims to be a simple dynamic DNS updater for an awesome free Finnish DDNS service, 
dy.fi. The script waits in a loop and executes every 60 seconds to check if the IP has changed.
If the IP has changed, it will post an update.  Also, if the time since last post to the DDNS 
has been exceeded, the script will also post an update. This is because dy.fi will drop the 
lease after 7 days of not pointing the IP.

## Motivation

The update script for dy.fi is written in Perl, and as I had no understanding in it and didn't get 
it to work without root user, I decided to write my own in Python.

## Configuration files:

### cfg/auth.ini

Program needs an `auth.ini` -file in directory cfg/, which holds the login credentials. For now, 
the only entry this program uses, is 'dy.fi'. Please fill your credentials in it. Template:

```
[dy.fi]
user = UserOrEmail
password = SuperSecretPassword
```

### cfg/config.ini
Template:
```
[URLs]
checkip_url = https://ipinfo.io/ip
update_url = https://www.dy.fi/nic/update
hostname = testdomain.dy.fi
refresh_time_seconds = 86400
```

Parameters:

`checkip_url`: url from which to check the current IP address \
`update_url`:  url to which send update request \
`hostname`:     your reserved domain to which you want to update the IP \
`refresh_time_seconds`: Time after which to send an update request, although IP has not changed. 


The script will also create a `cfg/history.json` for storing information about the last IP it 
updated and when did the update take presence.

## Podman / Docker

Dockerfile was developed using Podman, should work with Docker also but it's not tested. Just 
change each word "podman" to "docker" in `run.sh`. You also need to figure out how autostart in 
boot works for docker.

Image will be set to autostart with systemd. Remember to edit `cfg/auth.ini` and `cfg/config.ini` 
before deploying container!
```
chmod +x run.sh
./run.sh
```

Verify that container is running:
```
systemctl status --user container-ddns
```

Alternatively, for only running container once (no restart on boot):
```
podman build -t ddns -f Dockerfile
podman container run -d  --name ddns -it localhost/ddns
```
